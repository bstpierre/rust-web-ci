FROM ubuntu:18.04

ARG NIGHTLY_VERSION

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN export DEBIAN_FRONTEND=noninteractive \
 && export USER=root \
 && apt-get update \
 && apt-get install -y --no-install-recommends \
        build-essential=12.4ubuntu1 \
        ca-certificates=20180409 \
        curl=7.58.0-2ubuntu3.8 \
        libmysqlclient-dev=5.7.27-0ubuntu0.18.04.1 \
        libpq-dev=10.10-0ubuntu0.18.04.1 \
        libsqlite3-0=3.22.0-1ubuntu0.1 \
        libsqlite3-dev=3.22.0-1ubuntu0.1 \
        libssl-dev=1.1.1-1ubuntu2.1~18.04.4 \
        pkg-config=0.29.1-0ubuntu2 \
        sqlite3=3.22.0-1ubuntu0.1 \
 && curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y \
 && . "$HOME/.cargo/env" \
 && rustup toolchain install nightly${NIGHTLY_VERSION} \
 && rustup default nightly${NIGHTLY_VERSION} \
 && rustup component add clippy \
 && rustup component add rustfmt \
 && rustup target add wasm32-unknown-unknown \
 && cargo install diesel_cli \
 && cargo install wasm-pack \
 && cargo install cargo-make \
 && cargo install mdbook \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*
