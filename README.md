# rust-web-ci

This creates a docker container intended for use by CI in rust crates,
especially where webassembly is used.

Provides:

* ubuntu:18.04 base
* ubuntu build-essentials and other related packages
* rustup
* nightly toolchain
* wasm32 target
* wasm-pack
* cargo make
* clippy
* rustfmt
* diesel-cli
* mdbook
